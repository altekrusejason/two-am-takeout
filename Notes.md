This file is for taking notes as you go.

1.
first question was easy enough, there is a similar concept in java frameworks to customize the default behacior about how fields are serialized into JSON or other formats. Largely for the reasons discussed in the go article, mismatches in conventions in different languages.

https://stackoverflow.com/questions/52358247/what-are-golang-struct-field-naming-conventions

I guess based on this page on go conventions I might have also considered just changing the struct to have a property name of lowercase "ok" and remove the json string format thingy to have less code and make the frontend and backend refer to the same exact name everywhere.



2.
The second one is less clear to be "done" with, which I assume is deliberate. I did change it to send JSON through the websocket, but I'm not sure that really means much, its just text anyway. But the frontend code that interacte with the socket in in the frontend is pretty simple so I'm not sure there is much more to understand about the interaction.

     sendMessage: function(text) {
       const message = JSON.stringify({
         text,
         uuid: this.uuid
       })
       this.$socket.send(message)
     },

    this.$socket.onmessage = (message) => {
      const data = JSON.parse(message.data)
      data.timestamp = Date.now()
      this.messages.push(data)
    }

Going to move on for now and maybe come back to this.

3.
Adding another input was easy and putting it in the websocket message was straightforward.

Bonus question 1.

Given that there isn't a login system, I'm guessing the security issue was not enforcing Access-Control-Allow-Origin on websocket requests.

Bonus question 2.

Didn't do too much, added a conditional label over a series of message from one sender instead of labelling each message.

Also made the message box clear after sending.
